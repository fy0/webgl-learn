
var gl;
var points;
var sub_count = 5;
var degrees = 0;
var hollow = true;

function rot(p, radius) {
    var x = p[0], y = p[1];
    new_x = x * Math.cos(radius) - y * Math.sin(radius);
    new_y = x * Math.sin(radius) + y * Math.cos(radius);
    return vec2(new_x, new_y);
}

function twist(v, radius) {
    return rot(v, length(v)*radius);
}

function subdivision(a, b, c, count, degrees) {
    var degrees = degrees || 0;
    var mid_ab = mix(a, b, 0.5);
    var mid_ac = mix(c, a, 0.5);
    var mid_bc = mix(b, c, 0.5);
    var lst = [];

    if (count > 0) {
        count -= 1;
        lst = lst.concat(subdivision(mid_ab, b, mid_bc, count, degrees));
        lst = lst.concat(subdivision(a, mid_ab, mid_ac, count, degrees));
        lst = lst.concat(subdivision(mid_ac, mid_bc, c, count, degrees));
        if (!hollow) lst = lst.concat(subdivision(mid_ab, mid_bc, mid_ac, count, degrees));
    } else {
        var rad = radians(degrees);
        lst = lst.concat([twist(a, rad), twist(b, rad), twist(c, rad)]);
    }
    return lst;
}

var vertices_init = [
    vec2(-0.8660254037844387, -0.5),
    vec2(0, 1),
    vec2(0.8660254037844387, -0.5)
];

window.onload = function init()
{
    var canvas = document.getElementById("gl-canvas");
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert("WEBGL 不可用！"); }

    
    var vertices = subdivision(
        vertices_init[0], vertices_init[1], vertices_init[2], sub_count, degrees
    );

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    // 加载 shaders 和初始化属性buffer
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // 把数据加载进GPU
    var bufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW );


    // 将shader变量与数据buffer关联    
    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    render(vertices.length);
    
    document.getElementById("slider").onchange = function(ev) {
        sub_count = parseInt(ev.target.value);
        reset_subdivision(sub_count, degrees);
    };
    
    document.getElementById("slider-deg").onchange = function(ev) {
        degrees = parseInt(ev.target.value);
        reset_subdivision(sub_count, degrees);
    };
    
    document.getElementById("is_hollow").onchange = function(ev) {
        hollow = ev.target.checked;
        reset_subdivision(sub_count, degrees);
    };

};

function reset_subdivision(count, degrees) {
    var vertices = subdivision(
        vertices_init[0], vertices_init[1], vertices_init[2], count, degrees
    );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW );

    render(vertices.length);
}


function render(len)
{
    gl.clear( gl.COLOR_BUFFER_BIT );
    gl.drawArrays( gl.TRIANGLES, 0, len );
}
