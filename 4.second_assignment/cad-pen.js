
var gl;

var is_mouse_pressed = {
    0: false,
    1: false,
    2: false,
};

var start_count;
var count;
var current_linewidth = 1.0;
var current_color = [0, 0, 0, 1];
var vBuffer;
var lines = [];

function get_a_list(len) {
    var lst = [];
    for (var i=0;i<len;i++)
        lst.push(0);
    return lst;
}

window.onload = function init()
{
    var canvas = document.getElementById("gl-canvas");
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert("WEBGL 不可用！"); }

    
    var vertices = [
    ];

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    // 加载 shaders 和初始化属性buffer
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );
    
    // 片段着色器色彩
    fColor = gl.getUniformLocation( program, "fColor");

    // 随便填充一些数据
    vBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData( gl.ARRAY_BUFFER, flatten(get_a_list(sizeof['vec2']*65536)), gl.STATIC_DRAW );

    // 将shader变量与数据buffer关联 
    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    count = 0;    

    canvas.addEventListener("mousedown", function(event){
        is_mouse_pressed[event.button] = true;
        
        start_count = count;
    });

    canvas.addEventListener("mouseup", function(event){
        is_mouse_pressed[event.button] = false;

        lines.push([start_count, count-start_count, current_color, current_linewidth]);
        render();
    });
    
    $("#color_black").click(function() {
        current_color = [0,0,0,1];
    });
    
    $("#color_red").click(function() {
        current_color = [1,0,0,1];
    });
    
    $("#color_green").click(function() {
        current_color = [0,1,0,1];
    });
    
    $("#color_blue").click(function() {
        current_color = [0,0,1,1];
    });
    
    $("#slider").change(function(ev) {
        current_linewidth = ev.target.value;
        $("#current_slider").text(ev.target.value + ' px');
    });

    canvas.addEventListener("mousemove", function(ev){
        // fix for firefox
        ev.offsetX = ev.offsetX || ev.layerX;
        ev.offsetY = ev.offsetY || ev.layerY;
        
        var clip_x = -1 + 2*ev.offsetX / canvas.width;
        var clip_y = -1 + 2*(canvas.height-ev.offsetY) / canvas.height;

        $('#window-x').text(ev.offsetX);
        $('#window-y').text(ev.offsetY);

        $('#clip-x').text(clip_x.toFixed(5));
        $('#clip-y').text(clip_y.toFixed(5));

        if (is_mouse_pressed[0]) {
            //gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
            var v = vec2(clip_x, clip_y);
            gl.bufferSubData(gl.ARRAY_BUFFER, sizeof['vec2'] * count, flatten(v));
            count ++;
            render();
        }
    });
    
    //render();
};

function render()
{
    gl.clear( gl.COLOR_BUFFER_BIT );
    for (var i in lines) {
        var item = lines[i];
        gl.lineWidth( item[3] );
        gl.uniform4fv( fColor, flatten(item[2]) );
        gl.drawArrays( gl.LINE_STRIP, item[0], item[1] );
    }
    gl.lineWidth(current_linewidth);
    gl.uniform4fv( fColor, flatten(current_color) );
    gl.drawArrays( gl.LINE_STRIP, start_count, count-start_count);
}
